import Info from "./components/Info";

function App() {
  return (
    <div>
      <Info firstName="Vu Dang" lastName="Minh" favNumber={25}>Props children</Info>
    </div>
  );
}

export default App;
