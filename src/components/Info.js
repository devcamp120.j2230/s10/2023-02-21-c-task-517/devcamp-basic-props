import { Component } from "react";

class Info extends Component {
    render() {
        const { firstName, lastName, favNumber, children } = this.props;
        console.log(children);
        return (
            <p>My name is {firstName} {lastName} and my favourite number is {favNumber}.</p>
        )
    }
}

export default Info;